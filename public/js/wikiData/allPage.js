import listing from "./listing.js";

class allListings extends listing{
  constructor(title, url){
    super(title, [], [], "", url, [], false);
  }
  rebuildAll(pages, almost){
    let titles = [];
    let pageCount = 0;
    let altCount = 0;
    for(let i = 0; i < pages.length; i++){
      if(pages[i].title=="Blank")continue;
      titles.push({"title":pages[i].title, "url":pages[i].url, "real":true});
      pageCount++;
      for(let j = 0; j < pages[i].altListings.length; j++){
        titles.push({"title":pages[i].altListings[j].c, "url":pages[i].altListings[j].l, "real":false});
        altCount++;
      }
    }
    titles.sort(function(a, b){
        if(a.title.toUpperCase() < b.title.toUpperCase()) { return -1; }
        if(a.title.toUpperCase() > b.title.toUpperCase()) { return 1; }
        return 0;
    });
    let content;
    content = "<p>There are " + pageCount + " pages on this wiki. (Excluding special pages & redirects.)</p>";
    content += "<p>↪ indicates that it is a alternative name for a different page.</p>";
    content += "<ul>";
    if(almost){
      this.title = "Almost All Pages";
      content += "";
    } else {
      this.title = "All Pages";
      content += "<li>[Home]</li>";
      content += "<li>[404 Page|404]</li>";
      content += "<li>[All Pages|all-pages]</li>";
      content += "<li>[Almost All Pages|almost-all-pages]</li>";
    }
    for(let i = 0; i < titles.length; i++){
      if(titles[i].real){
        content += "<li>[" + titles[i].title + "|" + titles[i].url + "]</li>";
      } else {
        content += "<li>[" + titles[i].title + "|" + titles[i].url + "] ↪</li>";
      }
    }
    content += "</ul>"
    this.rawContent = content;
  }
}

const title = "All Pages";
const url = "all-pages";
let article = new allListings(title, url);
export default article;
