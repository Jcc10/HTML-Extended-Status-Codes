const rxLinksA = /\[([^\]\|\~]+)\|{0}\~{0}\]{0}\]/g;
const rxLinksB = /\[([^\]\|]+)\|([^\]]+)\]/g;
const rxLinksE = /\[([^\]\~]+)\~([^\]]+)\]/g;
const rxSmallH2 = /\*\*([^\*]+)\*~\*/g;
const rxH2 = /\*\*([^\*]+)\*\*([^\*]+)\*\*/g;

export default class wiki{
  constructor(wikiData, home, missingPage, allPage){
    this.pages = wikiData;
    this.container = document.querySelector("#wiki");
    this.article = document.querySelector("#wiki #article");
    this.home = home;
    this.missingPage = missingPage;
    this.allPage = allPage;
    this.container.querySelector("#goHome").addEventListener("click", (e)=>{
      e.preventDefault();
      this.view("home");
    });
    this.container.querySelector("#goAllPages").addEventListener("click", (e)=>{
      e.preventDefault();
      this.view("all-pages");
    });
    window.addEventListener('popstate', (e) => {
      if(e.state.page){
        this.view(e.state.page, true);
      } else {
        this.view("home", true);
      }
    })
  }
  updatePages(wikiData){
    this.pages = wikiData;
  }
  FindArticle(title){
    let page = false;
    switch(title){
      case "Home":
      case "home":
        page = this.home;
        break;
      case "404":
        page = this.missingPage;
        break;
      case "all-pages":
      case "all-page":
        page = this.allPage;
        page.rebuildAll(this.pages);
        break;
      case "almost-all-pages":
        page = this.allPage;
        page.rebuildAll(this.pages, true);
        break;
    }
    if(page)return page;
    
    let regexpSearch = new RegExp(("^" + title + "$"), "ig");
    for(let i = 0; i < this.pages.length; i++){
      if(this.pages[i].findListing(regexpSearch)){
        page = this.pages[i];
        break;
      }
    }
    if(page)
      return page;
    else
      return this.missingPage;
  }
  view(pageTitle, dontState){
    let page = this.FindArticle(pageTitle);
    this.display(page);
    this.attachTriggers();
    this.setPage(pageTitle, dontState);
  }
  display(page){
    let article = "";
    //The page Title
    {
      article = "<h1>" + page.title + "</h1>";
    }
    //The page Content
    {
      article += page.rawContent;
    }
    //The page See Also's
    if(page.seeAlso.length > 0){
      article += "**See also**See_also**<ul>";
      for(let i = 0; i < page.seeAlso.length; i++){
          article += "<li>" + page.seeAlso[i] + "</li>";
      }
      article += "</ul>";
    }
    //The page external links
    if(page.external.length > 0){
      article += "**External Links**External_links**<ul>";
      for(let i = 0; i < page.external.length; i++){
        article += "<li>" + page.external[i] + "</li>";
      }
      article += "</ul>";
    }
    if(page.uses){
      article += "<p>(This article incorporates text from " + page.uses + ".)</p>";
    }
    if(page.title == page.url){
      article += "<p><small>HTML link: [Link|" + page.url + "] <br/>MiniWiki link: &#x5B;" + page.title + "]</small></p>";
    } else {
      article += "<p><small>HTML link: [Link|" + page.url + "]<br/>MiniWiki link: &#x5B;" + page.title + "|" + page.url + "]</small></p>";
    }
    article = article.replace(rxLinksA, "[$1|$1]");
    article = article.replace(rxLinksE, "<a href='$2'>$1</a>");
    article = article.replace(rxLinksB, "<a href='./?page=$2' data-wikiLink='$2'>$1</a>");
    article = article.replace(rxSmallH2, "<h2><a name=$1>$1</a></h2>");
    article = article.replace(rxH2, "<h2><a name=$2>$1</a></h2>");
    this.article.innerHTML = article;
    this.article.scrollIntoView(); 
    document.title = "Wiki: " + page.title;
  }
  attachTriggers(){
    let links = this.article.querySelectorAll("a[data-wikiLink]");
    for(let i = 0; i < links.length; i++){
      links[i].addEventListener("click", (e) => {
        e.preventDefault();
        let dest = e.target.dataset["wikilink"];
        this.view(dest);
      });
    }
  }
  setPage(pageUrl, dontState){
    let url = new URL(window.location.href);
    let query_string = url.search;
    let search_params = new URLSearchParams(query_string); 
    // new value of "page" is set to the new url
    search_params.set('page', pageUrl);
    // change the search property of the main url
    url.search = search_params.toString();
    // the new url string
    let new_url = url.toString();
    if(dontState){
      window.history.replaceState({page:pageUrl}, null, new_url);
    } else{
      window.history.pushState({page:pageUrl}, null, new_url);
    }
  }
}
