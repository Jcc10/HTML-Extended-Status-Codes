import loadJson from './loadJson.js';

export function loadPages(){
  return new Promise(function(resolve, reject) {
    fetch(("./wiki/index.json"), {"method":"GET"}).then( resp => resp.json()).then( index => {
      
      let allJSONs = [];
      for (let i = 0; i < index.length; i++) {
        allJSONs.push(fetch(("./wiki/"+index[i]+".json"), {"method":"GET"}));
      }
      Promise
      .all(allJSONs)
      .then((response) => {
        let splitJSONS = [];
        for (let i = 0; i < response.length; i++) {
            splitJSONS.push(response[i].json());
        }
        return Promise.all(splitJSONS);
      })
      .then((jsons) => {
        let combinedJSON = [];
        for (let i = 0; i < jsons.length; i++) {
            combinedJSON = combinedJSON.concat(jsons[i])
        }
        return combinedJSON;
      })
      .then((data) => {
        resolve(loadJson(data));
      })
      .catch((error) => {
          console.warn(error);
      });
    });
  });
};

export {default as home} from './home.js';
export {default as missingPage} from './404.js';
export {default as allPage} from './allPage.js';
