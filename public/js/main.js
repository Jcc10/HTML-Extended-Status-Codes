import {loadPages, home, missingPage, allPage} from './wikiData/main.js';
import wikiClass from './wiki.js';

let wiki;

loadPages().then( wikiPages => {
  wiki = new wikiClass(wikiPages, home, missingPage, allPage);
  let params = {};
  {
    let url_string = window.location.href;
    let url = new URL(url_string);
    for(let pair of url.searchParams.entries()) {
       params[pair[0]] = pair[1];
    }
  }
  if(params["page"]){
    wiki.view(params["page"], true);
  } else {
    wiki.view("home", true);
  }
  window.wiki = wiki;
});

window.reloadPages = () => {
  loadPages().then( wikiPages => {
    wiki.updatePages(wikiPages);
  });
}
