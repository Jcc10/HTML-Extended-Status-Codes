import listing from "./listing.js";

const title = "404: Page not found.";
const alias = [];
const seeAlso = [];
const content = "<p>That page seems to not be here.</p><p>Go [home]?</p>";
let article = new listing(title, alias, seeAlso, content);
export default article;
