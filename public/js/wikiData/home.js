import listing from "./listing.js";

const title = "Home";
const alias = [];
const seeAlso = [];
const content = "<p>This is my own HTTP Status Code list since I didn't like the others.</p><p>Codes that are in the spec (as far as I know, I didn't actually read the RFC's) are listed with a * on them, codes that are not dont have the star.</p><ul><li>[1xx - Information responses|1xx]</li></ul>";
let article = new listing(title, alias, seeAlso, content);
export default article;
